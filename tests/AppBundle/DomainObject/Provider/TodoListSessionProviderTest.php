<?php

use AppBundle\DomainObject\Provider\TodoListSessionProvider;
use AppBundle\Entity\Todo;

/**
 * Created by PhpStorm.
 * User: johndoe
 * Date: 17/12/2015
 * Time: 13:44
 */
class TodoListSessionProviderTest extends \PHPUnit_Framework_TestCase
{
    public function testGetTodoList()
    {
        $todoList = array(
            '1' => new Todo('my title 1', 'my message 1', 1),
            '2' => new Todo('my title 2', 'my message 2', 3),
            '3' => new Todo('my title 3', 'my message 3', 3),
        );
        $session = $this
            ->getMock('Symfony\Component\HttpFoundation\Session\SessionInterface')
        ;

        $session
            ->expects($this->once())
            ->method('get')
            ->with($this->equalTo(TodoListSessionProvider::TODO_KEY), $this->anything())
            ->will($this->returnValue($todoList))
        ;

        $todoListProvider = new TodoListSessionProvider($session);

        $this->assertEquals($todoList, $todoListProvider->getTodoList());
        $this->assertEquals($todoList[2], $todoListProvider->find(2));
    }
}