<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testListWhenImNotConnected()
    {
        $client = self::createClient();
        $client->request('GET', '/todo/');

        $this->assertTrue($client->getResponse()->isRedirection());

        $client->followRedirect();

        $this->assertEquals('/login', $client->getRequest()->getRequestUri());
    }
}
