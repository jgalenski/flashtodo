<?php

/* AppBundle:User:login.html.twig */
class __TwigTemplate_ab59130d5914afae93ee879b3a308f76825884909d4b9b24be59318669532fe0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AppBundle::base.html.twig", "AppBundle:User:login.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'title' => array($this, 'block_title'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4efb0bc744dc7fe354fb2e375de27aad1714fde5e718ff11ce9321ba99b61cc7 = $this->env->getExtension("native_profiler");
        $__internal_4efb0bc744dc7fe354fb2e375de27aad1714fde5e718ff11ce9321ba99b61cc7->enter($__internal_4efb0bc744dc7fe354fb2e375de27aad1714fde5e718ff11ce9321ba99b61cc7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:User:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4efb0bc744dc7fe354fb2e375de27aad1714fde5e718ff11ce9321ba99b61cc7->leave($__internal_4efb0bc744dc7fe354fb2e375de27aad1714fde5e718ff11ce9321ba99b61cc7_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_5731f547db66acc9dad76372a84a595bbd53f62f9946536df0649bc6a34f7ba4 = $this->env->getExtension("native_profiler");
        $__internal_5731f547db66acc9dad76372a84a595bbd53f62f9946536df0649bc6a34f7ba4->enter($__internal_5731f547db66acc9dad76372a84a595bbd53f62f9946536df0649bc6a34f7ba4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form', array("action" => $this->env->getExtension('routing')->getPath("login_check")));
        echo "
";
        
        $__internal_5731f547db66acc9dad76372a84a595bbd53f62f9946536df0649bc6a34f7ba4->leave($__internal_5731f547db66acc9dad76372a84a595bbd53f62f9946536df0649bc6a34f7ba4_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_8498fce8b019dc636ba58ab93e02283bb5f9e3bb541f2621e31b153071926e5b = $this->env->getExtension("native_profiler");
        $__internal_8498fce8b019dc636ba58ab93e02283bb5f9e3bb541f2621e31b153071926e5b->enter($__internal_8498fce8b019dc636ba58ab93e02283bb5f9e3bb541f2621e31b153071926e5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "
";
        
        $__internal_8498fce8b019dc636ba58ab93e02283bb5f9e3bb541f2621e31b153071926e5b->leave($__internal_8498fce8b019dc636ba58ab93e02283bb5f9e3bb541f2621e31b153071926e5b_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:User:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  50 => 7,  41 => 4,  35 => 3,  11 => 1,);
    }
}
/* {% extends 'AppBundle::base.html.twig' %}*/
/* */
/* {% block body %}*/
/* {{ form(form, {action: path('login_check')}) }}*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/* */
/* {% endblock %}*/
/* */
/* */
