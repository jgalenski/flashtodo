<?php

/* AppBundle::base.html.twig */
class __TwigTemplate_a91e81ed7c30b24dc98e6d56a0071c38989efccb430d9e599ae636d3019d89eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fbd8dbd4918c13635273f610d1d9c3eb6b2ea4bc0a40e6cb013002e9593e1f52 = $this->env->getExtension("native_profiler");
        $__internal_fbd8dbd4918c13635273f610d1d9c3eb6b2ea4bc0a40e6cb013002e9593e1f52->enter($__internal_fbd8dbd4918c13635273f610d1d9c3eb6b2ea4bc0a40e6cb013002e9593e1f52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"UTF-8\" />
    <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
</head>
<body>
<h1>";
        // line 9
        $this->displayBlock("title", $context, $blocks);
        echo "</h1>
<div>
    ";
        // line 11
        echo twig_include($this->env, $context, "AppBundle::flash.html.twig");
        echo "
    ";
        // line 12
        $this->displayBlock('body', $context, $blocks);
        // line 13
        echo "</div>
</body>
</html>
";
        
        $__internal_fbd8dbd4918c13635273f610d1d9c3eb6b2ea4bc0a40e6cb013002e9593e1f52->leave($__internal_fbd8dbd4918c13635273f610d1d9c3eb6b2ea4bc0a40e6cb013002e9593e1f52_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_29fe34b2fbe27d766b47350dcb0902ee7355b96d3d07a17f626a2458efa85e1a = $this->env->getExtension("native_profiler");
        $__internal_29fe34b2fbe27d766b47350dcb0902ee7355b96d3d07a17f626a2458efa85e1a->enter($__internal_29fe34b2fbe27d766b47350dcb0902ee7355b96d3d07a17f626a2458efa85e1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Flash Todo";
        
        $__internal_29fe34b2fbe27d766b47350dcb0902ee7355b96d3d07a17f626a2458efa85e1a->leave($__internal_29fe34b2fbe27d766b47350dcb0902ee7355b96d3d07a17f626a2458efa85e1a_prof);

    }

    // line 12
    public function block_body($context, array $blocks = array())
    {
        $__internal_db3050aff2778e185adcafecab6d4aa370a6e86b8945d79058e8ae9628ee41f2 = $this->env->getExtension("native_profiler");
        $__internal_db3050aff2778e185adcafecab6d4aa370a6e86b8945d79058e8ae9628ee41f2->enter($__internal_db3050aff2778e185adcafecab6d4aa370a6e86b8945d79058e8ae9628ee41f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        echo "";
        
        $__internal_db3050aff2778e185adcafecab6d4aa370a6e86b8945d79058e8ae9628ee41f2->leave($__internal_db3050aff2778e185adcafecab6d4aa370a6e86b8945d79058e8ae9628ee41f2_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 12,  61 => 5,  51 => 13,  49 => 12,  45 => 11,  40 => 9,  34 => 6,  30 => 5,  24 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/* <head>*/
/*     <meta charset="UTF-8" />*/
/*     <title>{% block title 'Flash Todo' %}</title>*/
/*     <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/* </head>*/
/* <body>*/
/* <h1>{{ block('title') }}</h1>*/
/* <div>*/
/*     {{ include('AppBundle::flash.html.twig') }}*/
/*     {% block body '' %}*/
/* </div>*/
/* </body>*/
/* </html>*/
/* */
