<?php

/* AppBundle::flash.html.twig */
class __TwigTemplate_2f6ec0d9591fedebaa0b9beb14903984008aeb0d8f1a972804294205f80e380d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c9e2e29493ed4036673bf8ce0f5e687f8ec067088e71a8ade4e30863ca22448c = $this->env->getExtension("native_profiler");
        $__internal_c9e2e29493ed4036673bf8ce0f5e687f8ec067088e71a8ade4e30863ca22448c->enter($__internal_c9e2e29493ed4036673bf8ce0f5e687f8ec067088e71a8ade4e30863ca22448c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle::flash.html.twig"));

        // line 1
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "getFlashBag", array()), "has", array(0 => "success"), "method")) {
            // line 2
            echo "    <hr/>
";
        }
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "getFlashBag", array()), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["success"]) {
            // line 5
            echo "    <p style=\"color:green;background-color: deepskyblue;\">";
            echo twig_escape_filter($this->env, $context["success"], "html", null, true);
            echo "</p>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['success'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "<hr/>

";
        
        $__internal_c9e2e29493ed4036673bf8ce0f5e687f8ec067088e71a8ade4e30863ca22448c->leave($__internal_c9e2e29493ed4036673bf8ce0f5e687f8ec067088e71a8ade4e30863ca22448c_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle::flash.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 7,  32 => 5,  28 => 4,  24 => 2,  22 => 1,);
    }
}
/* {% if app.session.getFlashBag.has('success') %}*/
/*     <hr/>*/
/* {% endif %}*/
/* {% for success in app.session.getFlashBag.get('success') %}*/
/*     <p style="color:green;background-color: deepskyblue;">{{ success }}</p>*/
/* {% endfor %}*/
/* <hr/>*/
/* */
/* */
