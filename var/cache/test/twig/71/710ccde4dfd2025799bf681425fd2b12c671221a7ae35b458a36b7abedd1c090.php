<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_e88f0a09aab93466849fc57f80e91300d6a31de3b29d7fc5dfe5db07b4812003 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3a110a6dbed4b0253005f3b940dbfbe2e511af911b22bbaa75fa0c1d5c3ff2d = $this->env->getExtension("native_profiler");
        $__internal_b3a110a6dbed4b0253005f3b940dbfbe2e511af911b22bbaa75fa0c1d5c3ff2d->enter($__internal_b3a110a6dbed4b0253005f3b940dbfbe2e511af911b22bbaa75fa0c1d5c3ff2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b3a110a6dbed4b0253005f3b940dbfbe2e511af911b22bbaa75fa0c1d5c3ff2d->leave($__internal_b3a110a6dbed4b0253005f3b940dbfbe2e511af911b22bbaa75fa0c1d5c3ff2d_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_a82cf3f43731c12787d451b66c800cb93d89d9fd25270da7f6b3b2b096ee3b20 = $this->env->getExtension("native_profiler");
        $__internal_a82cf3f43731c12787d451b66c800cb93d89d9fd25270da7f6b3b2b096ee3b20->enter($__internal_a82cf3f43731c12787d451b66c800cb93d89d9fd25270da7f6b3b2b096ee3b20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_a82cf3f43731c12787d451b66c800cb93d89d9fd25270da7f6b3b2b096ee3b20->leave($__internal_a82cf3f43731c12787d451b66c800cb93d89d9fd25270da7f6b3b2b096ee3b20_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_90659a5a1a4b1eb963dac0bdbe75fe31bff05a7a7b4ef6c3d17580ffad8166ab = $this->env->getExtension("native_profiler");
        $__internal_90659a5a1a4b1eb963dac0bdbe75fe31bff05a7a7b4ef6c3d17580ffad8166ab->enter($__internal_90659a5a1a4b1eb963dac0bdbe75fe31bff05a7a7b4ef6c3d17580ffad8166ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_90659a5a1a4b1eb963dac0bdbe75fe31bff05a7a7b4ef6c3d17580ffad8166ab->leave($__internal_90659a5a1a4b1eb963dac0bdbe75fe31bff05a7a7b4ef6c3d17580ffad8166ab_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_303e562594a97e0c5c2639b4e40c62f1dccfba74d8bcf1b454b4daaad02425ac = $this->env->getExtension("native_profiler");
        $__internal_303e562594a97e0c5c2639b4e40c62f1dccfba74d8bcf1b454b4daaad02425ac->enter($__internal_303e562594a97e0c5c2639b4e40c62f1dccfba74d8bcf1b454b4daaad02425ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_303e562594a97e0c5c2639b4e40c62f1dccfba74d8bcf1b454b4daaad02425ac->leave($__internal_303e562594a97e0c5c2639b4e40c62f1dccfba74d8bcf1b454b4daaad02425ac_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
