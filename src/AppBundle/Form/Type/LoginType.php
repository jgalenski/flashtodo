<?php
/**
 * Created by PhpStorm.
 * User: johndoe
 * Date: 15/12/2015
 * Time: 13:37
 */

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Component\Validator\Constraints\NotBlank;

class LoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('_username', Type\TextType::class, array(
                'constraints' => array(
                    new NotBlank()
                )
            ))
            ->add('_password', Type\PasswordType::class, array(
                'constraints' => array(
                    new NotBlank()
                )
            ))
            ->add('Login', Type\SubmitType::class)
        ;
    }
}
