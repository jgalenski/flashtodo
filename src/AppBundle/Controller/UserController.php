<?php
/**
 * Created by PhpStorm.
 * User: johndoe
 * Date: 16/12/2015
 * Time: 16:39
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\UserType;
use AppBundle\Form\Type\LoginType;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @return array
     *
     * @Route("/register", name="user_register")
     * @Template
     */
    public function registerAction(Request $request)
    {
        $form = $this->createForm(UserType::class);
        if ($form->handleRequest($request)->isValid()) {
            $user = $form->getData();
            $user->encodePassword($this->get('security.password_encoder'));

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'User created successfully');

            return $this->redirectToRoute('user_register');
        }

        return array('form' => $form->createView());
    }

    /**
     * @return array
     *
     * @Route("/login", name="login")
     * @Template
     */
    public function loginAction()
    {
        $form = $this->createForm(LoginType::class);

        return array('form' => $form->createView());
    }
}
