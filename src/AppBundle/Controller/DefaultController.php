<?php

namespace AppBundle\Controller;

use AppBundle\DomainObject\Provider\TodoListSessionProvider;
use AppBundle\DomainObject\Provider\TodoListCookieProvider;
use AppBundle\Entity\Todo;
use AppBundle\Form\Type\TodoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 *
 * @Route("/todo")
 */
class DefaultController extends Controller
{
    /**
     * @Method("GET")
     * @Route("/", name="list_todo")
     * @Template
     *
     * @param Request $request
     * @return array
     */
    public function listAction(Request $request)
    {
        return array('todoList' => $this->get('app.todo.provider')->findAll());
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/new", name="new_todo")
     * @Template
     *
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function newAction(Request $request)
    {
        $form = $this->createForm(TodoType::class);

        if ($form->handleRequest($request)->isValid()) {
            $response = $this->redirectToRoute('list_todo');
            $this->get('app.todo.provider')->save($form->getData(), array('response' => $response));

            $this->addFlash('success', 'New todo '. $form->getData()->getId() .' added, with success');

            return $response;
        }

        return array('form' => $form->createView());
    }

    /**
     * @Method("GET")
     * @Route("/{id}", name="show_todo", requirements={"id": "\d+"})
     * @Template
     *
     * @param Request $request
     * @param $id
     * @return array
     */
    public function showAction(Request $request, $id)
    {
        return array('todo' => $this->get('app.todo.provider')->find($id));
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/{id}/edit", name="edit_todo", requirements={"id": "\d+"})
     * @Template
     *
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Request $request, $id)
    {
        $todo = $this->get('app.todo.provider')->find($id);
        $form = $this->createForm(TodoType::class, $todo);

        if ($form->handleRequest($request)->isValid()) {
            $response = $this->redirectToRoute('show_todo', array('id' => $todo->getId()));
            $this->get('app.todo.provider')->save($todo, array('response' => $response));

            $this->addFlash('success', 'Update todo '. $todo->getId() .', with success');

            return $response;
        }

        return array(
            'form' => $form->createView(),
            'todo' => $todo
        );
    }

    /**
     * @Method("GET")
     * @Route("/{id}/delete", name="delete_todo", requirements={"id": "\d+"})
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $todo = $this->get('app.todo.provider')->find($id);

        $this->get('app.todo.provider')->delete($todo);

        $response = $this->redirectToRoute('list_todo');
        $this->get('app.todo.provider')->save(null, array('response' => $response));

        $this->addFlash('success', 'Deleted todo '. $id .', with success');

        return $response;
    }

    /**
     * @param $id
     * @return \AppBundle\Entity\Todo
     */
    protected function getTodo($id)
    {
        $em = $this->getDoctrine()->getManager();
        $rep = $em->getRepository('AppBundle:Todo');

        $todo = $rep->find($id);
        if (!$todo) {
            throw $this->createNotFoundException("No todo found...");
        }
        return $todo;
    }
}
