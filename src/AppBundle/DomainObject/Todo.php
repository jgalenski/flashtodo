<?php
/**
 * Created by PhpStorm.
 * User: johndoe
 * Date: 15/12/2015
 * Time: 13:35
 */

namespace AppBundle\DomainObject;

use Symfony\Component\Validator\Constraints as Assert;

class Todo
{
    public function __construct()
    {
        $this->id = uniqid();
    }

    public $id;

    /**
     * @Assert\NotBlank
     */
    public $title;

    /**
     * @Assert\NotBlank
     */
    public $message;
}