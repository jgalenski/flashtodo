<?php
/**
 * Created by PhpStorm.
 * User: johndoe
 * Date: 15/12/2015
 * Time: 13:44
 */

namespace AppBundle\DomainObject\Provider;

use AppBundle\Entity\Todo;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class TodoListSessionProvider extends AbstractTodoListProvider
{
    private $session;

    /**
     * TodoProvider constructor.
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
        $this->todoList = $this->session->get(self::TODO_KEY, array());
    }

    public function save(Todo $todo = null, $options = array())
    {
        if ($todo && !$todo->getId()) {
            $todo->generateId();
        }

        parent::save($todo);

        $this->session->set(self::TODO_KEY, $this->todoList);
    }
}