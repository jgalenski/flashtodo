<?php
/**
 * Created by PhpStorm.
 * User: johndoe
 * Date: 15/12/2015
 * Time: 13:44
 */

namespace AppBundle\DomainObject\Provider;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use AppBundle\Entity\Todo;

/**
 * Class AbstractTodoListProvider
 * @package AppBundle\DomainObject\Provider
 */
abstract class AbstractTodoListProvider
{
    /**
     *
     */
    const TODO_KEY = 'flash_todo';

    /**
     * @var array
     */
    protected $todoList = array();

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->todoList;
    }

    /**
     * @param $id
     * @return null
     */
    public function find($id)
    {
        if(isset($this->todoList[$id])) {
            return $this->todoList[$id];
        }

        return null;
    }

    /**
     * @param Todo $todo
     */
    public function delete(Todo $todo)
    {
        unset($this->todoList[$todo->getId()]);
    }

    /**
     * @param Todo|null $todo
     * @param array $options
     */
    public function save(Todo $todo = null, $options = array())
    {
        if ($todo) {
            $this->todoList[$todo->getId()] = $todo;
        }
    }

    /**
     * @return array
     */
    public function getTodoList()
    {
        return $this->todoList;
    }
}
