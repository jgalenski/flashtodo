<?php
/**
 * Created by PhpStorm.
 * User: johndoe
 * Date: 15/12/2015
 * Time: 13:44
 */

namespace AppBundle\DomainObject\Provider;

use AppBundle\Entity\Todo;
use Doctrine\Bundle\DoctrineBundle\Registry;

class TodoListDoctrineProvider extends AbstractTodoListProvider
{
    private $em;
    private $rep;

    /**
     * TodoProvider constructor.
     * @param Registry $doctrine
     */
    public function __construct(Registry $doctrine)
    {
        $this->em = $doctrine->getManager();
        $this->rep = $this->em->getRepository('AppBundle:Todo');
    }

    public function findAll()
    {
        return $this->rep->findAll();
    }

    public function find($id)
    {
        return $this->rep->find($id);
    }

    public function delete(Todo $todo)
    {
        $this->em->remove($todo);
    }

    public function save(Todo $todo = null, $options = array())
    {
        if ($todo) {
            $this->em->persist($todo);
        }

        $this->em->flush();
    }
}