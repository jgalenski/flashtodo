<?php
/**
 * Created by PhpStorm.
 * User: johndoe
 * Date: 15/12/2015
 * Time: 13:44
 */

namespace AppBundle\DomainObject\Provider;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Todo;

class TodoListCookieProvider extends AbstractTodoListProvider
{
    /**
     * TodoProvider constructor.
     * @param RequestStack $rs
     */
    public function __construct(RequestStack $rs)
    {
        $request = $rs->getCurrentRequest();
        $this->todoList = unserialize($request->cookies->get(self::TODO_KEY));
    }

    public function save(Todo $todo = null, $options = array())
    {
        if ($todo && !$todo->getId()) {
            $todo->generateId();
        }

        parent::save($todo);

        if (isset($options['response']) && $options['response'] instanceof Response) {
            $response = $options['response'];
            $response->headers->setCookie($this->getCookie());
        }
    }

    public function getCookie()
    {
        return new Cookie(self::TODO_KEY, serialize($this->todoList));
    }
}
